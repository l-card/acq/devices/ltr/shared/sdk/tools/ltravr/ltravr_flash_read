#include "ltr/include/ltravrapi.h"
#include "getopt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include "WinSock2.h"
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#define OPT_HELP       'h'
#define OPT_CRATE_SN   'c'
#define OPT_SLOT       's'
#define OPT_SRV_ADDR    0x100
#define OPT_SIZE        0x101


#define ERR_INVALID_USAGE           -1



static const struct option f_long_opt[] = {
    {"help",         no_argument,       0, OPT_HELP},
    /* вывод сообщений о процессе загрузки */
    {"csn",          required_argument, 0, OPT_CRATE_SN},
    {"slot",         required_argument, 0, OPT_SLOT},
    {"srv-addr",     required_argument, 0, OPT_SRV_ADDR},
    {"size",         required_argument, 0, OPT_SIZE},
    {0,0,0,0}
};

static const char* f_opt_str = "hc:s:";

typedef struct {
    int slot;
    const char *csn;
    unsigned read_size;
    DWORD srv_addr;
} t_options;

static struct {
    WORD mid;
    const char *name;
    INT size;
} f_param_tbl[] = {
    {LTR_MID_LTR11,  "LTR11",  ATMEGA8515_FLASH_SIZE},
    {LTR_MID_LTR114, "LTR114", ATMEGA128_FLASH_SIZE},
    {LTR_MID_LTR22,  "LTR22",  ATMEGA8515_FLASH_SIZE},
    {LTR_MID_LTR27,  "LTR27",  ATMEGA8515_FLASH_SIZE},
    {LTR_MID_LTR41,  "LTR41",  ATMEGA8515_FLASH_SIZE},
    {LTR_MID_LTR42,  "LTR42",  ATMEGA8515_FLASH_SIZE},
    {LTR_MID_LTR43,  "LTR43",  ATMEGA8515_FLASH_SIZE},
    {LTR_MID_LTR51,  "LTR51",  ATMEGA8515_FLASH_SIZE},
};

static const char* f_usage_descr = \
"\nUsage: ltravr_flash_read [OPTIONS]\n\n" \
" ltravr_flash_read is command line utility for read AVR flash content\n" \
"    of LTR-modules with AVR controller.\n" \
" Typical usage:\n\n" \
"   ltravr_flash_read --slot=slot-in-crate > dump_file_name\n\n";

static const char* f_options_descr =
"Options:\n" \
"-c, --csn=crate-serial  - Crate serial with interested module\n"
"-h, --help              - Print this help and exit\n"
"    --size              - Flash content size in 16-bit words\n"
"-s, --slot=module-slot  - Slot number (1-16) of interested module (default 1)\n"
"    --srv-addr=addr     - ltrd ip-address if you want to connect to remote\n"
"                            instance of ltrd\n\n";

static void  f_print_usage(void) {
    unsigned int i;
    fprintf(stdout, "%s", f_usage_descr);
    fprintf(stdout, "\nSupported modules:\n");
    for (i=0; i < sizeof(f_param_tbl)/sizeof(f_param_tbl[0]); i++) {
        fprintf(stdout, "    %s\n", f_param_tbl[i].name);
    }
    fprintf(stdout,"\n");
    fprintf(stdout, "%s", f_options_descr);
}


static int f_parse_options(t_options* st, int argc, char **argv, int* out) {
    int err = 0;
    int opt = 0;

    *out = 0;
    memset(st, 0, sizeof(t_options));
    st->csn = "";
    st->slot = LTR_CC_CHNUM_MODULE1;

    /************************** разбор опций *****************************/
    while ((opt!=-1) && !err && !*out) {
        opt = getopt_long(argc, argv, f_opt_str,
                          f_long_opt, 0);
        switch (opt) {
            case -1:
                break;
            case OPT_HELP:
                f_print_usage();
                *out = 1;
                break;
            case OPT_CRATE_SN:
                st->csn = optarg;
                break;
            case OPT_SLOT: {
                    int slot = atoi(optarg);
                    if ((slot < LTR_CC_CHNUM_MODULE1) || (slot > LTR_CC_CHNUM_MODULE16)) {
                        fprintf(stderr, "Error! Invalid slot number specified\n");
                        err = ERR_INVALID_USAGE;
                    } else {
                        st->slot = slot;
                    }
                }
                break;
            case OPT_SIZE:
                sscanf(optarg, "%i", &st->read_size);
                if ( st->read_size <= 0) {
                    fprintf(stderr, "Error! Invalid flash read size specified\n");
                    err = ERR_INVALID_USAGE;
                }
                break;
            case OPT_SRV_ADDR:
                st->srv_addr = inet_addr(optarg);
                if ((st->srv_addr==INADDR_NONE) || (st->srv_addr==INADDR_ANY)) {
                    fprintf(stderr, "Invalid ltrd IP-address specified!\n");
                    err = ERR_INVALID_USAGE;
                }
                break;
            default:
                break;
        }
    }

    return err;
}



int main(int argc, char** argv) {
    INT err=LTR_OK;
    t_options par;
    int out = 0;

    err = f_parse_options(&par, argc, argv, &out);

    if (!err && !out) {
        TLTRAVR hnd;
        LTRAVR_Init(&hnd);
        /* устанавливаем соединение с модулем находящемся в первом слоте крейта.
           для сетевого адреса, сетевого порта ltr-сервера и серийного номера
           крейта используем значения по умолчанию */
        err=LTRAVR_Open(&hnd, par.srv_addr, LTRD_PORT_DEFAULT, par.csn, par.slot);
        if (err!=LTR_OK) {
           fprintf(stderr, "Cannot open module. Error %d (%s)\n",
                     err, LTRAVR_GetErrorString(err));
        } else {
            INT close_err;
            WORD *read_arr = NULL;
            DWORD size = par.read_size;

            printf("Open module: crate = %s, slot = %d!\n", hnd.ltr.csn, hnd.ltr.cc);

            if (size == 0)
                size = ATMEGA8515_FLASH_SIZE;

            read_arr = malloc(size*sizeof(read_arr[0]));
            if (read_arr == NULL) {
                err = LTR_ERROR_MEMORY_ALLOC;
                fprintf(stderr, "Cannot allocate memory for read array!\n");
            } else {
                err = LTRAVR_ReadProgrammMemory(&hnd, read_arr,size, 0);
                if (err!=LTR_OK) {
                     fprintf(stderr, "Cannot read flash-memory.Error %d (%s)\n",
                             err, LTRAVR_GetErrorString(err));
                } else {
                    unsigned i;
                    for (i=0; i < size; i++) {
                        printf("%4d: 0x%04X\n", i, read_arr[i]);
                    }
                }
            }

            close_err = LTRAVR_Close(&hnd);
            if (close_err!=LTR_OK) {
                fprintf(stderr, "Cannot close module. Error %d (%s)\n",
                                 close_err, LTRAVR_GetErrorString(close_err));
            }

        }
    }

    return err;
}

